package com.vimage.taskscontentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.vimage.taskscontentprovider.data.TaskContract;
import com.vimage.taskscontentprovider.data.TaskDBHelper;

/**
 * Created by Dimon on 20.12.2016. Тут будет работа с БД тасков
 */
public class TaskContentProvider extends ContentProvider {
    final String LOG_TAG = "myLogs";

    static final String AUTHORITY = "com.vimage.providers.Tasks";

    // path
    static final String TASK_PATH = "tasks";

    // Общий Uri
    public static final Uri TASK_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + TASK_PATH);

    // Типы данных
    // набор строк
    static final String TASK_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + TASK_PATH;

    // одна строка
    static final String TASK_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + TASK_PATH;

    //// UriMatcher
    // общий Uri
    static final int URI_TASKS = 1;

    // Uri с указанным ID
    static final int URI_TASKS_ID = 2;

    // описание и создание UriMatcher
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, TASK_PATH, URI_TASKS);
        uriMatcher.addURI(AUTHORITY, TASK_PATH + "/#", URI_TASKS_ID);

    }

    private TaskDBHelper taskDbHelper;

    @Override
    public boolean onCreate() {
        taskDbHelper = new TaskDBHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        // проверяем Uri
        //Log.d(LOG_TAG, uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_TASKS: // общий Uri

                // если сортировка не указана, ставим свою - по идентификатору - по умолчанию будет в порядке вставки.
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = TaskContract.TaskEntry._ID + " ASC";
                }
                break;

            case URI_TASKS_ID: // Uri с ID
                String id = uri.getLastPathSegment();

                // добавляем ID к условию выборки
                if (TextUtils.isEmpty(selection)) {
                    selection = TaskContract.TaskEntry._ID + " = " + id;
                } else {
                    selection = selection + " AND " + TaskContract.TaskEntry._ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }

        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE_NAME, projection, selection,
                selectionArgs, null, null, sortOrder);
        // просим ContentResolver уведомлять этот курсор
        // об изменениях данных в TASK_CONTENT_URI
        cursor.setNotificationUri(getContext().getContentResolver(),
                TASK_CONTENT_URI);
        return cursor;

    }

    @Nullable
    @Override
    public String getType(Uri uri) {
         switch (uriMatcher.match(uri)) {
            case URI_TASKS:
                return TASK_CONTENT_TYPE;
            case URI_TASKS_ID:
                return TASK_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        //Log.d(LOG_TAG, "insert, " + uri.toString());
        if (uriMatcher.match(uri) != URI_TASKS)
            throw new IllegalArgumentException("Wrong URI: " + uri);

        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        long rowID = db.insert(TaskContract.TaskEntry.TABLE_NAME, null, contentValues);
        Uri resultUri = ContentUris.withAppendedId(TASK_CONTENT_URI, rowID);
        // уведомляем ContentResolver, что данные по адресу resultUri изменились
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        switch (uriMatcher.match(uri)) {
            case URI_TASKS:
                //Log.d(LOG_TAG, "URI_delete");
                break;
            case URI_TASKS_ID:
                String id = uri.getLastPathSegment();

                if (TextUtils.isEmpty(selection)) {
                    selection = TaskContract.TaskEntry._ID + " = " + id;
                } else {
                    selection = selection + " AND " + TaskContract.TaskEntry._ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        int cnt = db.delete(TaskContract.TaskEntry.TABLE_NAME, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        switch (uriMatcher.match(uri)) {
            case URI_TASKS:
                 break;
            case URI_TASKS_ID:
                String id = uri.getLastPathSegment();
                //Log.d(LOG_TAG, "URI_CONTACTS_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = TaskContract.TaskEntry._ID + " = " + id;
                } else {
                    selection = selection + " AND " + TaskContract.TaskEntry._ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        int cnt = db.update(TaskContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }
}

